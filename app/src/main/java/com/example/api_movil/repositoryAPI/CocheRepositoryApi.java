package com.example.api_movil.repositoryAPI;

import com.example.api_movil.entities.Coche;
import com.example.api_movil.entities.Usuario;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface CocheRepositoryApi {


    // Para poder agregar un usuario ala BBDD por medio de la API
    @POST("agregar")
    Call<Usuario> agregarCoche(@Body Coche coche);


    // Obtiene una lista de registros de usuarios
    @GET("mostrar")
    Call<ArrayList<Coche>> obtenerListaCoche();



}
