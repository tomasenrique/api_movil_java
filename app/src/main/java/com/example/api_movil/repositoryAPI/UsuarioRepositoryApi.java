package com.example.api_movil.repositoryAPI;

import com.example.api_movil.entities.Usuario;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface UsuarioRepositoryApi {

    // Para poder agregar un usuario ala BBDD por medio de la API
    @POST("agregar")
    Call<Usuario> agregarUsuario(@Body Usuario usuario);

    // Obtiene la lista de usuarios de la BBDD por medio de la API
    @GET("mostrar")
    Call<ArrayList<Usuario>> obtenerListaUsuarios();

    // Obtiene un usuario por medio de su dni
    @GET("mostrar/dni/{dni}")
    Call<Usuario> obtenerUsuarioDni(@Path("dni") String dni);

    // Obtiene un susario por medio de su email
    @GET("mostrar/email/{email}")
    Call<Usuario> obtenerUsuarioEmial(@Path("email") String email);

    // Actualiza los datos de un usuario por medio de su dni
    @PUT("actualizar")
    Call<Usuario>actualizarUsuario(@Body Usuario usuario);










}
