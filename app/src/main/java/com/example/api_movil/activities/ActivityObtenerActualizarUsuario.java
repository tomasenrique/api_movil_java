package com.example.api_movil.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.api_movil.R;
import com.example.api_movil.conexion.Api;
import com.example.api_movil.conexion.Enlace;
import com.example.api_movil.entities.Usuario;
import com.example.api_movil.repositoryAPI.UsuarioRepositoryApi;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ActivityObtenerActualizarUsuario extends AppCompatActivity {

    private static final String TAG = "ACTUALIZAR USUARIO"; //  Para mostrar mensajes por consola
    private Retrofit retrofitActualizarUsuario; // Clase para poder realizar la conexion a la API
    private Button buttonBuscarUsuario, buttonLimpiarCamposUsuario, buttonActualizarUsuario; // Agrega un usuario a la BBDD
    private EditText editTextNombre, editTextApellido, editTextDni, editTextEmial, editTextTelefono;

    private Usuario usuarioRespuesta; // Para poder realizar las actualizaciones


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_obtener_actualizar_usuario);

        // Se declara los campos para mostrar ypoder editar los datos
        editTextNombre = findViewById(R.id.editTextActualizarNombre);
        editTextApellido = findViewById(R.id.editTextActualizarApellido);
        editTextDni = findViewById(R.id.editTextActualizarDNI);
        editTextEmial = findViewById(R.id.editTextActualizarEmail);
        editTextTelefono = findViewById(R.id.editTextActualizarTelefono);

        // Botones para realizar acciones de busqueda y actualizacion
        buttonBuscarUsuario = findViewById(R.id.buttonBuscarUsuario);
        buttonLimpiarCamposUsuario = findViewById(R.id.buttonLimpiar);
        buttonActualizarUsuario = findViewById(R.id.buttonActualizarUsuario);

        // Conexiones a la API
        Enlace enlace = new Enlace(); // para obtener los enlaces de conexion a la api
        Api api = new Api(); // para obtener la conexion a la API
        retrofitActualizarUsuario = api.getConexion(enlace.getLink(enlace.USER));
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    public void onResume() {
        super.onResume();

        // Para buscar un usuario
        buttonBuscarUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buscarUsuario(); // buscar los datos de un usuario
            }
        });

        // Para limpiar los campos
        buttonLimpiarCamposUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiarCampos(); // limpia los campos editText
            }
        });

        // Para actualizar los datos
        buttonActualizarUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actualizarUsuario(); // actualiza los datos de un usuario
            }
        });

    }

    public void onRestart() {
        super.onRestart();

    }


    public void onPause() {
        super.onPause();

    }

    public void onStop() {
        super.onStop();

    }

    public void onDestroy() {
        super.onDestroy();

    }

    // =============================================================================================
    // =============================================================================================

    private void limpiarCampos() {
        editTextNombre.setText("");
        editTextApellido.setText("");
        editTextDni.setText("");
        editTextEmial.setText("");
        editTextTelefono.setText("");
    }

    private void buscarUsuario() {
        String dniUsuario = editTextDni.getText().toString().trim(); // obtiene el dni de usuario
        UsuarioRepositoryApi usuarioRepositoryApi = retrofitActualizarUsuario.create(UsuarioRepositoryApi.class);
        Call<Usuario> buscarUsuario = usuarioRepositoryApi.obtenerUsuarioDni(dniUsuario); // buscar el usuario

        buscarUsuario.enqueue(new Callback<Usuario>() {
            @Override
            public void onResponse(Call<Usuario> call, Response<Usuario> response) {
                usuarioRespuesta = response.body();

                if (usuarioRespuesta != null) { // verifica que exite el dato para que la app no se cuelgue
                    // Se obtiene los datos de la API
                    String nombreRespuesta = usuarioRespuesta.getNombre();
                    String apellidoRespuesta = usuarioRespuesta.getApellido();
                    String dniRespuesta = usuarioRespuesta.getDni();
                    String emialRespuesta = usuarioRespuesta.getEmail();
                    String telefonoRespuesta = usuarioRespuesta.getTelefono();

                    // Se asigna los datos obtenidos
                    editTextNombre.setText(nombreRespuesta);
                    editTextApellido.setText(apellidoRespuesta);
                    editTextDni.setText(dniRespuesta);
                    editTextEmial.setText(emialRespuesta);
                    editTextTelefono.setText(telefonoRespuesta);

                } else { // si no existe muestra el mensaje
                    Toast toast = Toast.makeText(getApplicationContext(), "No existe el usuario", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.TOP, 0, 1400);
                    toast.show();
                }
            }

            @Override
            public void onFailure(Call<Usuario> call, Throwable t) {
                Log.e(TAG, " => ERROR EN LA CONEXION BUSQUEDA USUARIO => onFailure: " + t.getMessage());
            }
        });
    }

    private void actualizarUsuario() {
        Usuario usuarioActualizado = new Usuario(); // objeto vacio para almacenar datos para actualizar
        usuarioActualizado.setId(usuarioRespuesta.getId()); // obtiene el id de usuario buscar para luego actualizar
        // aqui se obtiene los datos modificados de los campos editText
        usuarioActualizado.setNombre(editTextNombre.getText().toString());
        usuarioActualizado.setApellido(editTextApellido.getText().toString());
        usuarioActualizado.setDni(editTextDni.getText().toString());
        usuarioActualizado.setEmail(editTextEmial.getText().toString());
        usuarioActualizado.setTelefono(editTextApellido.getText().toString());
        // A partir de aqui se realiza la actualizacion
        UsuarioRepositoryApi usuarioRepositoryApi = retrofitActualizarUsuario.create(UsuarioRepositoryApi.class);
        Call<Usuario> actualizar = usuarioRepositoryApi.actualizarUsuario(usuarioActualizado); // Se aplica el objeto usuario con los datos actualizados

        actualizar.enqueue(new Callback<Usuario>() {
            @Override
            public void onResponse(Call<Usuario> call, Response<Usuario> response) {
                Usuario usuarioActualizado = response.body(); // obtiene los nuevos datos modificados de la API
                String nombreActualizado = usuarioActualizado.getNombre();
                String apellidoActualizado = usuarioActualizado.getApellido();
                String dniActualizado = usuarioActualizado.getDni();
                String emialActualizado = usuarioActualizado.getEmail();
                String telefonoActualizado = usuarioActualizado.getTelefono();
                // Se asigna los datos modificados obtenidos para mostrarlos en la vista
                editTextNombre.setText(nombreActualizado);
                editTextApellido.setText(apellidoActualizado);
                editTextDni.setText(dniActualizado);
                editTextEmial.setText(emialActualizado);
                editTextTelefono.setText(telefonoActualizado);
            }

            @Override
            public void onFailure(Call<Usuario> call, Throwable t) {
                Log.e(TAG, " => ERROR DE CONEXION ACTUALIZAR PARA USUARIO => onFailure: " + t.getMessage());
            }
        });


    }


}
