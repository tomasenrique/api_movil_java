package com.example.api_movil.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import com.example.api_movil.R;
import com.example.api_movil.adaptadores.UsuariosAdapter;
import com.example.api_movil.conexion.Api;
import com.example.api_movil.conexion.Enlace;
import com.example.api_movil.entities.Usuario;
import com.example.api_movil.repositoryAPI.UsuarioRepositoryApi;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Esta clase se encargar de mostra en la vista los datos obtenidos de la base de datos obtenidos por
 * medio del uso de la API
 */
public class ActivityMostrarUsuarios extends AppCompatActivity {

    private static final String TAG = "USUARIO"; //  Para mostrar mensajes por consola
    private Retrofit retrofit; // Clase para poder realizar la conexion a la API
    private RecyclerView recyclerView; // Para mostrar los datos como lista
    private UsuariosAdapter usuariosAdapter; // Para aplicar el diseño personalizado de lista a la vista

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mostrar_usuarios);

        Enlace enlace = new Enlace(); // para obtener los enlaces de conexion a la api
        Api api = new Api(); // para obtener la conexion a la API
        retrofit = api.getConexion(enlace.getLink(enlace.USER));

        // Se ubica la lista de tipo RecyclerView
        recyclerView = findViewById(R.id.recyclerViewListaUsuarios);
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    public void onResume() {
        super.onResume();

        // Llama al metodo para obtener los datos de los usuarios de la API
        obtenerDatosUsuarios();
    }

    public void onRestart() {
        super.onRestart();

    }


    public void onPause() {
        super.onPause();

    }

    public void onStop() {
        super.onStop();

    }

    public void onDestroy() {
        super.onDestroy();

    }

    // =============================================================================================
    // =============================================================================================

    private void obtenerDatosUsuarios() {
        // Se instancia la interfaz y se le aplica el objeto(retrofit) con la conexion para obtener los datos.
        UsuarioRepositoryApi usuarioRepositoryApi = retrofit.create(UsuarioRepositoryApi.class);
        // Se realiza la llamada al metodo para obtener los datos y se almacena la respuesta aqui.
        Call<ArrayList<Usuario>> usuarioAnswerCall = usuarioRepositoryApi.obtenerListaUsuarios();

        // Aqui se realiza la solicitud al servidor de forma asincrónicamente y se obtiene 2 respuestas.
        usuarioAnswerCall.enqueue(new Callback<ArrayList<Usuario>>() {
            // Aqui nos indicara si se realiza una conexion, y esta puede tener 2 tipos de ella
            @Override
            public void onResponse(Call<ArrayList<Usuario>> call, Response<ArrayList<Usuario>> response) {
                // Si la conexion es exitosa esta mostrara la informacion obtenida de la base de datos
                if (response.isSuccessful()) {
                    // Obtiene todos los datos de la BBDD por medio de la api y se almacena en el arrayList
                    ArrayList<Usuario> usuarioAnswer = response.body();

                    /**Se instancia la clase para el diseño personalizado y se le pasa como parametro 'getApplicationContext()' que
                     * sera el contexto ==>> 'context' que se podra usar para agregar posibles nuevos diseños
                     * y se le para tambien el arrayList con los datos obtenidos para aplicar a la vista diseñada */
                    usuariosAdapter = new UsuariosAdapter(usuarioAnswer, getApplicationContext()); //
                    // Se aplica el diseño de la vista a la lista RecyclerView con los datos agregados en el.
                    recyclerView.setAdapter(usuariosAdapter);
                    // Se aplica el tamaño del recycleView para poder mostrar la vista en pantalla.
                    recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext())); // OJO, sin esto no se mostrara

                    // Para mostrar por consola los nombres obtenidos por medio de la api a la BBDD, SOLO INFO DE PRUEBA se puede borrar.
                    for (int i = 0; i < usuarioAnswer.size(); i++) {
                        Log.e(TAG, "Usuario: " + usuarioAnswer.get(i).getNombre());
                    }
                } else { // Si no nos mostrara un mensaje indicando el tipo de error
                    Log.e(TAG, " ERROR AL CARGAR USUARIOS: onResponse" + response.errorBody());
                }
            }

            // Aqui, se mostrara si la conexion a la API falla.
            @Override
            public void onFailure(Call<ArrayList<Usuario>> call, Throwable t) {
                Log.e(TAG, " => ERROR LISTA USUARIO => onFailure: " + t.getMessage());
            }
        });

    }


}
