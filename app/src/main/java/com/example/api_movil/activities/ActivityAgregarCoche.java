package com.example.api_movil.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.api_movil.BuildConfig;
import com.example.api_movil.R;

import java.io.File;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class ActivityAgregarCoche extends AppCompatActivity {

    private final String CARPETA_RAIZ = "misImagenesApi/";  // aqui es donde se guardara las fotos
    private final String RUTA_IMAGEN = CARPETA_RAIZ + "misFotos"; // esta ruta permitira cargar la imagen desde el movil
    final int COD_SELECCIONA = 10; // Es para seleccionar la foto creada
    final int COD_FOTO = 20; // Con esto permitiremos que las imagenes se almacenen en la galeria
    private String path;  // Es la direccion donde se guardara la imagen

    private Button buttonCargarFoto, buttonAgregarCoche;
    private ImageView imageViewCoche; // Para poner la foto
    private EditText editTextTipoCoche, editTextModeloCoche, editTextAnyoCoche, editTextMatriculaCoche, editTextDniUsuarioCoche;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_coche);

        // Se declara los botones
        buttonCargarFoto = findViewById(R.id.buttonAgregarFotoCoche);
        buttonAgregarCoche = findViewById(R.id.buttonAgregarCoche);
        //Se declara las entradas de datos editText
        editTextTipoCoche = findViewById(R.id.editTextAgregarCocheTipo);
        editTextModeloCoche = findViewById(R.id.editTextAgregarCocheModelo);
        editTextAnyoCoche = findViewById(R.id.editTextAgregarCocheAnyo);
        editTextMatriculaCoche = findViewById(R.id.editTextAgregarCocheMatricula);
        editTextDniUsuarioCoche = findViewById(R.id.editTextAgregarCocheDniUsuario);
        // Se declara la vista para la foto
        imageViewCoche = findViewById(R.id.imagenCoche);

        // aqui se valida los permisos para usar la lectura y escritura de la camara y de la memoria
        if (validaPermisos()) {
            buttonCargarFoto.setEnabled(true);
        } else {
            buttonCargarFoto.setEnabled(false);
        }


    }

    @Override
    public void onStart() {
        super.onStart();

    }

    public void onResume() {
        super.onResume();

        buttonCargarFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cargarImagen(); // Carga una foto en la vista
            }
        });

    }

    public void onRestart() {
        super.onRestart();

    }


    public void onPause() {
        super.onPause();

    }

    public void onStop() {
        super.onStop();

    }

    public void onDestroy() {
        super.onDestroy();

    }

    // =============================================================================================
    // =============================================================================================


    private void cargarImagen() {
        // opciones a mostrar cuando se preciona el boton de la camara
        //final CharSequence[] opciones = {"Tomar Foto","Cargar Imagen","Cancelar"};
        final CharSequence[] opciones = {"Tomar Foto","Cargar Imagen"};
        // muestra las opciones por medio de una mensaje de tipo AlerDialog
        final AlertDialog.Builder alertOpciones = new AlertDialog.Builder(ActivityAgregarCoche.this);
        alertOpciones.setTitle("Seleccione una Opción"); // titulo del AlerDialog
        // aqui se agregan las opciones al AlerDialog
        alertOpciones.setItems(opciones, new DialogInterface.OnClickListener() {
            @Override // esto son las opciones al hacer click del boton camara
            public void onClick(DialogInterface dialogInterface, int i) {
                if (opciones[i].equals("Tomar Foto")){
                    tomarFotografia(); //llama a la funcion para tomar la foto
                }else{ // si no
                    if (opciones[i].equals("Cargar Imagen")){ // carga una imagen
                        Intent intent = new Intent(Intent.ACTION_GET_CONTENT, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        intent.setType("image/");// direccion donde se encuentran las imagenes
                        startActivityForResult(intent.createChooser(intent,"Seleccione la Aplicación"),COD_SELECCIONA);
                    }else{
                        dialogInterface.dismiss();
                    }
                }
            }
        });
        alertOpciones.show(); // muestra las opciones como AlerDialog
    }

    private void tomarFotografia() {
        // Aqui se guardara la foto, y la cargara
        File fileImagen = new File(Environment.getExternalStorageDirectory(),RUTA_IMAGEN);
        boolean isCreada = fileImagen.exists();  // nos dira si fue creada la imagen
        String nombreImagen = "";
        if(isCreada == false){ // si el caso que no se ha creado la imagen
            isCreada=fileImagen.mkdirs();// Esto la creara la carpeta
        }
        if(isCreada == true){// si la imagen esta creada
            //Guardara el archivo con el siguente nombre y formato, (System.currentTimeMillis()/1000) generara el tiempo para que el nombre sea diferente
            nombreImagen = (System.currentTimeMillis()/1000)+".jpg"; // devolvera la hora actual en milisegundos mas la extencion
        }
        // esto sera la ruta donde se guardara la imagen, File.separator = /
        path = Environment.getExternalStorageDirectory()+ File.separator + RUTA_IMAGEN + File.separator + nombreImagen;
        //System.out.println("===> el path: " + path);
        File imagen = new File(path); // se guardara el archivo creado

        Intent intent = null;
        intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            String authorities = getApplicationContext().getPackageName() + ".provider";
            //Uri imageUri = FileProvider.getUriForFile(this,authorities,imagen); // Desactualizado, solo funciona hasta la API 23
            Uri imageUri = FileProvider.getUriForFile(getApplicationContext(), BuildConfig.APPLICATION_ID + ".provider", imagen);
            //System.out.println("===> el imageUri: " + imageUri);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        }else{
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(imagen));
        }
        startActivityForResult(intent,COD_FOTO);
    }

    private boolean validaPermisos() {
        // Para verificar la version del sistema
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }

        // Esto validara los permisos de lectura y escritura necesarios para usar la camara
        if ((checkSelfPermission(CAMERA) == PackageManager.PERMISSION_GRANTED) && (checkSelfPermission(WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)) {
            return true;
        }

        // Esto validara el permiso de escritura y lectura en la memoria
        if ((shouldShowRequestPermissionRationale(CAMERA)) || (shouldShowRequestPermissionRationale(WRITE_EXTERNAL_STORAGE))) {
            cargarDialogoRecomendacion();
        } else {
            requestPermissions(new String[]{
                    WRITE_EXTERNAL_STORAGE, CAMERA
            }, 100);
        }
        return false;
    }


    private void solicitarPermisosManual() {
        final CharSequence[] opciones = {"si","no"}; // opciones, puede ser mas
        final AlertDialog.Builder alertOpciones = new AlertDialog.Builder(getApplicationContext());
        alertOpciones.setTitle("¿Desea configurar los permisos de forma manual?");
        alertOpciones.setItems(opciones, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (opciones[i].equals("si")){
                    Intent intent = new Intent();
                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    Uri uri = Uri.fromParts("package",getPackageName(),null);
                    intent.setData(uri);
                    startActivity(intent);
                }else{
                    Toast.makeText(getApplicationContext(),"Los permisos no fueron aceptados",Toast.LENGTH_SHORT).show();
                    dialogInterface.dismiss();
                }
            }
        });
        alertOpciones.show();
    }

    private void cargarDialogoRecomendacion() {
        AlertDialog.Builder dialogo = new AlertDialog.Builder(getApplicationContext());
        dialogo.setTitle("Permisos Desactivados");
        dialogo.setMessage("Debe aceptar los permisos para el correcto funcionamiento de la App");

        dialogo.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE,CAMERA},100);
            }
        });
        dialogo.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

//         con esto cargamos la imagen de la galeria
        if (resultCode == RESULT_OK){
            switch (requestCode){
                case COD_SELECCIONA:  // es para seleccionar la foto creada
                    Uri miPath = data.getData();
                    //Log.i("===> miPath<====", " " + miPath); // direccion de ubicacion
                    imageViewCoche.setImageURI(miPath);
                    break;

                case COD_FOTO: // con esto permitiremos que las imagenes se almacenen en la galeria
                    MediaScannerConnection.scanFile(this, new String[]{path}, null,
                            new MediaScannerConnection.OnScanCompletedListener() {
                                @Override
                                public void onScanCompleted(String path, Uri uri) {
                                    // este metodo nos dira si el proceso ya termino
                                    Log.i("Ruta de almacenamiento","Path: " + path);
                                }
                            });
                    //Se crea la imagen de la foto tomada y asignarla al imageView
                    Bitmap bitmap = BitmapFactory.decodeFile(path);
                    imageViewCoche.setImageBitmap(bitmap);
                    break;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // aqui se verifican los permisos
        if (requestCode == 100) {
            if (grantResults.length == 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                buttonCargarFoto.setEnabled(true);
            } else {
                solicitarPermisosManual(); // sirve para activar los permisos de manera manual en la app
            }
        }
    }

}
