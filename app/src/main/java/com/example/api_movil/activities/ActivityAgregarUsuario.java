package com.example.api_movil.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.example.api_movil.MainActivity;
import com.example.api_movil.R;
import com.example.api_movil.conexion.Api;
import com.example.api_movil.conexion.Enlace;
import com.example.api_movil.entities.Usuario;
import com.example.api_movil.repositoryAPI.UsuarioRepositoryApi;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ActivityAgregarUsuario extends AppCompatActivity {

    private static final String TAG = "AGREGAR USUARIO"; //  Para mostrar mensajes por consola

    private Retrofit retrofitAgregarUsuario; // Clase para poder realizar la conexion a la API

    private Button agregarUsuario; // Agrega un usuario a la BBDD
    private EditText nombre, apellido, dni, emial, telefono;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_usuario);

        agregarUsuario = findViewById(R.id.buttonAgregarUsuario); // Boton de agregar
        // Campos editText para insertar datos
        nombre = findViewById(R.id.editTextNombre);
        apellido = findViewById(R.id.editTextApellido);
        dni = findViewById(R.id.editTextDNI);
        emial = findViewById(R.id.editTextEmail);
        telefono = findViewById(R.id.editTextTelefono);


        Enlace enlace = new Enlace(); // para obtener los enlaces de conexion a la api
        Api api = new Api(); // para obtener la conexion a la API
        retrofitAgregarUsuario = api.getConexion(enlace.getLink(enlace.USER));
    }


    @Override
    public void onStart() {
        super.onStart();

    }

    public void onResume() {
        super.onResume();

        agregarUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                obtenerDatosUsuario();

                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });

    }

    public void onRestart() {
        super.onRestart();

    }


    public void onPause() {
        super.onPause();

    }

    public void onStop() {
        super.onStop();

    }

    public void onDestroy() {
        super.onDestroy();

    }

    // =============================================================================================
    // =============================================================================================

    private void obtenerDatosUsuario() {
        // Se obtienen los datos de los campos Editext
        String agregarNombre = nombre.getText().toString().trim(); // trim para eliminar espacion vacios adelante y atras
        String agregarApellido = apellido.getText().toString().trim();
        String agregarDni = dni.getText().toString().trim();
        String agregarEmail = emial.getText().toString().trim();
        String agregarTelefono = telefono.getText().toString().trim();

        // aqui se verifica que los campos no esten vacios
        if(agregarNombre.isEmpty()){
            nombre.setError("Falta agregar el nombre.");
            nombre.requestFocus();
        }

        if(agregarApellido.isEmpty()){
            apellido.setError("Falta agregar el apellido.");
            apellido.requestFocus();
        }

        if(agregarDni.isEmpty()){
            dni.setError("Falta agregar el DNI.");
            dni.requestFocus();
        }

        if(agregarEmail.isEmpty()){
            emial.setError("Falta agregar el email.");
            emial.requestFocus();
        }

        if(agregarTelefono.isEmpty()){
            telefono.setError("Falta agregar el telefono.");
            telefono.requestFocus();
        }
        // Se llama al metodo que agregara los datos a la BBDD por medio de la API
        agregarUsuario(agregarNombre, agregarApellido, agregarDni, agregarEmail, agregarTelefono);
    }


    public void agregarUsuario(final String nombre, final String apellido, final String dni, final String emial, final String telefono) {
        UsuarioRepositoryApi usuarioRepositoryApi = retrofitAgregarUsuario.create(UsuarioRepositoryApi.class);
        Usuario usuario = new Usuario(nombre, apellido, dni, emial, telefono);
        Call<Usuario> agregar = usuarioRepositoryApi.agregarUsuario(usuario);

        agregar.enqueue(new Callback<Usuario>() {
            @Override
            public void onResponse(Call<Usuario> call, Response<Usuario> response) {

                try { // Mostrara por consola la respuesta de la API de lo que se a agregado al servidor
                    Usuario usuarioPost = response.body();
                    // sera el codigo 201 que indica que se ha creado el usuario
                    Log.e(TAG, " => => Code: " + response.code());
                    Log.e(TAG, " => => Nombre: " + usuarioPost.getId());
                    Log.e(TAG, " => => Apellido: " + usuarioPost.getNombre());
                    Log.e(TAG, " => => Dni: " + usuarioPost.getApellido());
                    Log.e(TAG, " => => Emial: " + usuarioPost.getDni());
                    Log.e(TAG, " => => Telefono: " + usuarioPost.getEmail());
                    Log.e(TAG, " => => Telefono: " + usuarioPost.getTelefono());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Usuario> call, Throwable t) {
                Log.e(TAG, " => ERROR DE CONEXION PARA USUARIO => onFailure: " + t.getMessage());
            }
        });


    }

}


//    Toast toast = Toast.makeText(this, "Ventana 1 Estoy en onDestroy", Toast.LENGTH_LONG);
//        toast.setGravity(Gravity.TOP | Gravity.LEFT, 0, 1400);
//                toast.show();
