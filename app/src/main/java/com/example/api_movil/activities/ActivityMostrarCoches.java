package com.example.api_movil.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import com.example.api_movil.R;
import com.example.api_movil.adaptadores.CochesAdapter;
import com.example.api_movil.conexion.Api;
import com.example.api_movil.conexion.Enlace;
import com.example.api_movil.entities.Coche;
import com.example.api_movil.repositoryAPI.CocheRepositoryApi;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ActivityMostrarCoches extends AppCompatActivity {

    private static final String TAG = "COCHE"; //  Para mostrar mensajes por consola
    private Retrofit retrofitCoche;
    private RecyclerView recyclerViewCoche;
    private CochesAdapter cochesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mostrar_coches);

        Enlace enlace = new Enlace(); // para obtener los enlaces de conexion a la api
        Api api = new Api(); // para obtener la conexion a la API
        retrofitCoche = api.getConexion(enlace.getLink(enlace.CAR));

        recyclerViewCoche = findViewById(R.id.recyclerViewListaCoches);
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    public void onResume() {
        super.onResume();

        obtenerDatosCoches(); // llama a la lista de coches

    }

    public void onRestart() {
        super.onRestart();

    }


    public void onPause() {
        super.onPause();

    }

    public void onStop() {
        super.onStop();

    }

    public void onDestroy() {
        super.onDestroy();

    }

    // =============================================================================================
    // =============================================================================================

    private void obtenerDatosCoches(){

        CocheRepositoryApi cocheRepositoryApi = retrofitCoche.create(CocheRepositoryApi.class);
        Call<ArrayList<Coche>>cocheAnswerCall = cocheRepositoryApi.obtenerListaCoche();

        cocheAnswerCall.enqueue(new Callback<ArrayList<Coche>>() {
            @Override
            public void onResponse(Call<ArrayList<Coche>> call, Response<ArrayList<Coche>> response) {
                if (response.isSuccessful()){
                    ArrayList<Coche> cocheAnswer = response.body();
                    cochesAdapter = new CochesAdapter(cocheAnswer,getApplicationContext());
                    recyclerViewCoche.setAdapter(cochesAdapter);
                    recyclerViewCoche.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                    // Para ver si aparece la url de la imagen
                    for (int i = 0; i < cocheAnswer.size(); i++) {
                        Log.e(TAG, "Coche: " + cocheAnswer.get(i).getImagen());
                    }

                }else {
                    Log.e(TAG, " ERROR AL CARGAR COCHES: onResponse" + response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Coche>> call, Throwable t) {
                Log.e(TAG, " => ERROR LISTA COCHES => onFailure: " + t.getMessage());
            }
        });









    }



}
