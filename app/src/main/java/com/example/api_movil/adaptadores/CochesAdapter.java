package com.example.api_movil.adaptadores;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.api_movil.R;
import com.example.api_movil.entities.Coche;

import java.io.File;
import java.util.ArrayList;

public class CochesAdapter extends RecyclerView.Adapter<CochesAdapter.CochesHolder> {

    private static final String TAG = "RUTA"; //  Para mostrar mensajes por consola

    private ArrayList<Coche> listaCochesAdapter; // PAra almacenar los coches obtenidos de la BBDD
    private Context context;

    // Builder
    public CochesAdapter(ArrayList<Coche> listaCochesAdapter, Context context) {
        this.listaCochesAdapter = listaCochesAdapter;
        this.context = context;
    }

    //==============================================================================================
    @Override
    public CochesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.diseno_lista_coche, parent, false);
        RecyclerView.LayoutParams layoutParams = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        view.setLayoutParams(layoutParams);
        return new CochesHolder(view);
    }

    @Override
    public void onBindViewHolder(CochesHolder holder, int position) {

        holder.id.setText(String.valueOf(listaCochesAdapter.get(position).getId()));
        holder.tipo.setText(listaCochesAdapter.get(position).getTipo());
        holder.modelo.setText(listaCochesAdapter.get(position).getModelo());
        holder.anyo.setText(String.valueOf(listaCochesAdapter.get(position).getAnyo()));
        holder.matricula.setText(listaCochesAdapter.get(position).getMatricula());
        holder.id_usuario.setText(String.valueOf(listaCochesAdapter.get(position).getId_usuario().getId())); // obtiene el id del usuario del coche
//        holder.imagen.setImageURI(listaCochesAdapter.get(position).getImagen());



        /// PARA ARREGLAR - FALTA OBTENER LA IMAGEN
        File ruta = new File(listaCochesAdapter.get(position).getImagen());
        Log.e(TAG, " => RUTA IMAGEN LLEGADA => : " + ruta);

        String localhost = "http://192.168.1.23:8080";

        String rutaTotal = localhost + ruta.getAbsolutePath();
        Log.e(TAG, " => RUTA IMAGEN TOTAL => : " + rutaTotal);


        Glide.with(context)
                .load(rutaTotal)
                .error(R.mipmap.ic_launcher) //Muestra imagen por defecto si no carga la imagen de red
                .centerCrop() // La imagen ocupara todo el espacion disponible
                .crossFade() // Carga la imagen con una animacion
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.imagen); // aqui carga la imagen de la red



//        file:///C:/D/Lenguajes/MUESTRAS_DE_TRABAJOS/API_MOVIL_JAVA/api_escritorio_movil/api/imagen/camion.jpg


//        http://192.168.1.23:8080/api/coche/
//        C:\D\Lenguajes\MUESTRAS_DE_TRABAJOS\API_MOVIL_JAVA\api_escritorio_movil\api\src\main\resources\imagen

//        @Override
//        public void onBindViewHolder(ViewHolder holder, int position) {
//            Pokemon p = dataset.get(position);
//
//            //Esta parte obtiene los datos de tipo texto
//            holder.nombreTextView.setText(p.getName());
//
//            // esta parte obtiene la imagen del pokemon
//            Glide.with(context)
//                    .load("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + p.getNumber() + ".png")
//                    .centerCrop()
//                    .crossFade()
//                    .diskCacheStrategy(DiskCacheStrategy.ALL)
//                    .into(holder.fotoImageView);
//        }

    }

    @Override
    public int getItemCount() {
        return listaCochesAdapter.size();
    }

    //==============================================================================================


    public class CochesHolder extends RecyclerView.ViewHolder {
        private TextView id, tipo, modelo, anyo, matricula, id_usuario; // variables para cada vista
        private ImageView imagen;

        public CochesHolder(View itemView) {
            super(itemView);
            id = itemView.findViewById(R.id.textViewIdCoche);
            tipo = itemView.findViewById(R.id.textViewTipoCoche);
            modelo = itemView.findViewById(R.id.textViewModeloCoche);
            anyo = itemView.findViewById(R.id.textViewAnyoCoche);
            matricula = itemView.findViewById(R.id.textViewMatriculaCoche);
            id_usuario = itemView.findViewById(R.id.textViewIdUsuarioCoche);
            imagen = itemView.findViewById(R.id.imageViewImagenCoche);
        }
    }


}
