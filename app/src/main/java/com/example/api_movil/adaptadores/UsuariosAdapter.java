package com.example.api_movil.adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.api_movil.R;
import com.example.api_movil.entities.Usuario;

import java.util.ArrayList;

/**
 * Esta clase se encargara de gestionar(adaptar) como aplicar los datos obtenidos de la API a la vista de
 * diseñada que se mostrara en el movil.
 */

public class UsuariosAdapter extends RecyclerView.Adapter<UsuariosAdapter.UsuariosHolder> {

    private ArrayList<Usuario> listaUsuariosAdapter;  // Para almacenar los datos obtenidos de la bse de datos
    private Context context;  // Para podder usar metodos que solo estan en la actividad principal(modificar mas tarde para posible uso)

    // Builder
    public UsuariosAdapter(ArrayList<Usuario> listaUsuariosAdapter, Context context) {
        this.listaUsuariosAdapter = listaUsuariosAdapter;
        this.context = context;
    }

    //==============================================================================================
    // Metodos sobrescritos heredados de la herencia en esta clase
    @Override
    public UsuariosHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        /**Aqui se aplica el diseño personalizado a la vista para mostrar los datos, archivo con
         * diseño ubicado en la carpeta 'layout' ==>> diseno_lista_usuario.xml  */
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.diseno_lista_usuario, parent, false);
        /**Aqui se asigna el espacio que ocupara cada registro obtenido de la base de datos en la lista del RecyclerView
         * esto dependera del diseño personalizado que se haga   */
        RecyclerView.LayoutParams layoutParams = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        view.setLayoutParams(layoutParams);  // Se aplica los parametros dados en la variable 'layoutParams'
        return new UsuariosHolder(view); // Retorna la vista diseñada con los parametros dados y usando la clase interna en esta
    }

    @Override
    public void onBindViewHolder(UsuariosHolder holder, int position) {
        // Se aplican a las vistas los datos obtenido del arrayList segun su posicion en esta.
        holder.id.setText(String.valueOf(listaUsuariosAdapter.get(position).getId())); // Se parsea el dato para que se muestre como texto
        holder.nombre.setText(listaUsuariosAdapter.get(position).getNombre());
        holder.apellido.setText(listaUsuariosAdapter.get(position).getApellido());
        holder.dni.setText(listaUsuariosAdapter.get(position).getDni());
        holder.email.setText(listaUsuariosAdapter.get(position).getEmail());
        holder.telefono.setText(listaUsuariosAdapter.get(position).getTelefono());
    }

    @Override
    public int getItemCount() {
        // Con esto se contara la cantidad de objetos a obtener para mostrar en la lista de la vista
        return listaUsuariosAdapter.size();
    }
    //==============================================================================================

    // Clase interna para adartar los datos a la vista de movil
    public class UsuariosHolder extends RecyclerView.ViewHolder {
        private TextView id, nombre, apellido, dni, email, telefono; // variables para cada vista

        public UsuariosHolder(View itemView) {
            super(itemView);
            /**Campos de la vista diseñada para mostrar los datos obtenidos de la api, archivo en la
             * carpeta 'layout' =>> diseno_lista_usuario.xml              */
            id = itemView.findViewById(R.id.textViewId);
            nombre = itemView.findViewById(R.id.textViewNombre);
            apellido = itemView.findViewById(R.id.textViewApellido);
            dni = itemView.findViewById(R.id.textViewDni);
            email = itemView.findViewById(R.id.textViewEmail);
            telefono = itemView.findViewById(R.id.textViewTelefono);
        }
    }

}
