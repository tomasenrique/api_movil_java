package com.example.api_movil.conexion;

import java.util.HashMap;

/**
 * Esta clase se encargara de guardar los enlaces que ubicaran los metodos de las clases controller
 * para poder realizar el CRUD a la base de datos.
 */

public class Enlace {

    private final String LOCALHOST = "http://192.168.1.23:";  // Direccion Ip del Pc(puede variar)
    private final String PORT = "8080";  // Puerto de conexion, depende de la configuracion del Pc
    private final String LINK_USER = LOCALHOST + PORT + "/api/usuario/"; // Enlace de ubicacion de metodos CRUD de usuario
    private final String LINK_CAR = LOCALHOST + PORT + "/api/coche/";  // Enlace de ubicacion de metodos CRUD de coches
    // Claves para busqueda de link en el hashmap
    public final String USER = "user";
    public final String CAR = "car";

    private HashMap<String, String> enlacesUsuarioUrl; // Almacenara los enlaces o url para accedera a la api

    // Builder
    public Enlace() {
        // Inicializa el hashMap para tener disponibli los enlaces
        enlacesUsuarioUrl = new HashMap<>();
        enlacesUsuarioUrl.put("user", LINK_USER);
        enlacesUsuarioUrl.put("car", LINK_CAR);
    }


    //Method

    /**
     * Este metodo devolvera un string que sera la la direccion enlace donde se llama al metodo.
     * para realizar el CRUD a la base de datos.
     *
     * @param keyLink Sera la clave de tipo string para obtener el valor del enlace.
     * @return Devuelve una cadena de texto que sera el link para realizar llamara a la api.
     */
    public String getLink(String keyLink) {
        return enlacesUsuarioUrl.get(keyLink);
    }

}
