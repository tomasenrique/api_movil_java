package com.example.api_movil;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.api_movil.activities.ActivityAgregarCoche;
import com.example.api_movil.activities.ActivityAgregarUsuario;
import com.example.api_movil.activities.ActivityMostrarCoches;
import com.example.api_movil.activities.ActivityMostrarUsuarios;
import com.example.api_movil.activities.ActivityObtenerActualizarUsuario;

public class MainActivity extends AppCompatActivity {

    private Button agregarUsuario, mostrarUsuarios, buscarActualizarUsuario;
    private Button agregarCoche, mostrarCoches;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        agregarUsuario = findViewById(R.id.buttonPrincipalAgregarUsuario);
        mostrarUsuarios = findViewById(R.id.buttonPrincipalMostrarUsuarios);
        buscarActualizarUsuario = findViewById(R.id.buttonPrincipalBuscarActualizarUsuario);


        agregarCoche = findViewById(R.id.buttonPrincipalAgregarCoche);
        mostrarCoches = findViewById(R.id.buttonPrincipalMostrarCoches);

    }

    @Override
    public void onStart() {
        super.onStart();

    }

    public void onResume() {
        super.onResume();

        // Para a la pantalla para ingresar un usuarioa la base de datos
        agregarUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ActivityAgregarUsuario.class);
                startActivity(intent);
            }
        });


        // Para mostrar todos los datos de los usuarios
        mostrarUsuarios.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ActivityMostrarUsuarios.class);
                startActivity(intent);
            }
        });

        // Para ir a la pantalla de buscar y actualizar usuario
        buscarActualizarUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ActivityObtenerActualizarUsuario.class);
                startActivity(intent);
            }
        });








        // Para a la pantalla para ingresar un coche la base de datos
        agregarCoche.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ActivityAgregarCoche.class);
                startActivity(intent);
            }
        });

        // Para mostrar todos los datos de los coches
        mostrarCoches.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ActivityMostrarCoches.class);
                startActivity(intent);
            }
        });

    }

    public void onRestart() {
        super.onRestart();

    }


    public void onPause() {
        super.onPause();

    }

    public void onStop() {
        super.onStop();

    }

    public void onDestroy() {
        super.onDestroy();

        /*
        Toast toast = Toast.makeText(this, "Ventana 1 Estoy en onDestroy", Toast.LENGTH_LONG);
        toast.setGravity(Gravity.TOP | Gravity.LEFT, 0, 1400);
        toast.show();
        Log.i(AVISO, getLocalClassName() + ".onDestroy;"); */
    }
}
