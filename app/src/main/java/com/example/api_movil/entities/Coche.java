package com.example.api_movil.entities;

import java.io.Serializable;

public class Coche implements Serializable {

    private Long id;
    private String tipo; // Sera el tipo de vehiculo
    private String modelo; // Sera el modelo del coche
    private int anyo; // Sera le año de creacion del coche
    private String matricula; // Sera la matricula del coche
    private String imagen; // Sera la url para obtener la imagen
    private Usuario id_usuario; // Sera el id de usuario al que pertenece el coche

    // Builders
    public Coche() {
    }

    public Coche(String tipo, String modelo, int anyo, String matricula, String imagen, Usuario id_usuario) {
        this.tipo = tipo;
        this.modelo = modelo;
        this.anyo = anyo;
        this.matricula = matricula;
        this.imagen = imagen;
        this.id_usuario = id_usuario;
    }

    // Setter and Getter
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public int getAnyo() {
        return anyo;
    }

    public void setAnyo(int anyo) {
        this.anyo = anyo;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public Usuario getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(Usuario id_usuario) {
        this.id_usuario = id_usuario;
    }
}
